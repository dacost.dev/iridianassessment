import { StyleSheet, TouchableHighlight, View, Text } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

export default function Navbar({ navigation }) {
  return (
    <View style={styles.navBar}>
      <Text style={styles.title}>Event List</Text>
      <TouchableHighlight onPress={() => navigation.navigate("Bets")}>
        <MaterialIcons name="payments" size={24} color="#c9c9c9" />
      </TouchableHighlight>
    </View>
  );
}

const styles = StyleSheet.create({
  navBar: {
    display: "grid",
    justifyContent: "space-between",
    gridAutoFlow: "column",
    backgroundColor: "#142021",
    padding: "21px",
    position: "fixed",
    top: 0,
    width: "100%",
    zIndex: 1,
    borderBottomLeftRadius: "15px",
    borderBottomRightRadius: "15px",
  },
  title: { color: "chocolate" },
});

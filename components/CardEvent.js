import { MaterialIcons } from "@expo/vector-icons";
import { Bets } from "../App";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Image,
} from "react-native";

export default function CardEvent() {
  const { betsSelection, setBetsSelection, data, setData } = Bets();

  return (
    <>
      {data.map((item, index) => {
        if (item.markets.length > 0) {
          return (
            <View key={item.name + index}>
              <View style={styles.card}>
                <View style={styles.cardHeader}>
                  <Text style={styles.cardHeaderText}>{item.name}</Text>
                  <MaterialIcons
                    name="sports-soccer"
                    size={21}
                    color="chocolate"
                  />
                </View>

                <View style={styles.cardBody}>
                  {item.markets.map((market, marketIndex) => {
                    return (
                      <View
                        key={market.name + marketIndex}
                        style={styles.cardBodyBetsOptions}
                      >
                        <hr style={styles.hr} />
                        <View style={styles.headerBetsOptions}>
                          <Text style={styles.cardHeaderMarket}>
                            {market.name}
                          </Text>
                          <MaterialIcons
                            name="emoji-events"
                            size={21}
                            color="chocolate"
                          />
                        </View>

                        <View style={styles.btnBets}>
                          {market.selections.map(
                            (selection, selectionIndex) => {
                              return (
                                <View key={selection.name + selectionIndex}>
                                  <TouchableHighlight
                                    onPress={() => {
                                      if (
                                        data[index].markets[marketIndex]
                                          .selections[selectionIndex]
                                          .selected === undefined ||
                                        data[index].markets[marketIndex]
                                          .selections[selectionIndex]
                                          .selected === false
                                      ) {
                                        data[index].markets[
                                          marketIndex
                                        ].selections[
                                          selectionIndex
                                        ].selected = true;
                                        setBetsSelection([
                                          ...betsSelection,
                                          {
                                            eventName: item.name,
                                            eventIndex: index,
                                            marketName: market.name,
                                            marketIndex: marketIndex,
                                            price: selection.price,
                                            imageUrl: selection.imageUrl,
                                            selectionIndex: selectionIndex,
                                            selectionName: selection.name,
                                          },
                                        ]);
                                        setData([...data]);
                                      }
                                    }}
                                    underlayColor="transparent"
                                  >
                                    <View
                                      style={
                                        selection.selected === true
                                          ? styles.optionSelected
                                          : styles.options
                                      }
                                    >
                                      <View style={styles.optionImg}>
                                        <Image
                                          source={selection.imageUrl}
                                          style={{ width: 55, height: 55 }}
                                        />
                                        <Text style={styles.cardHeaderText}>
                                          {selection.name}
                                        </Text>
                                      </View>
                                      <Text style={styles.cardHeaderText}>
                                        {selection.price}
                                      </Text>
                                    </View>
                                  </TouchableHighlight>
                                  <View style={styles.vr} />
                                </View>
                              );
                            }
                          )}
                        </View>
                      </View>
                    );
                  })}
                </View>
              </View>
            </View>
          );
        }
      })}
    </>
  );
}

const styles = StyleSheet.create({
  cardHeaderText: { color: "#c9c9c9" },
  headerBetsOptions: {
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    gap: "13px",
  },
  card: {
    color: "white",
    backgroundColor: "#142021",
    padding: "21px",
    borderRadius: "10px",
  },
  hr: {
    width: "100%",
    marginTop: "21px",
    marginBottom: "21px",
    borderColor: "#797979",
  },
  btnBets: {
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "space-between",
    alignItems: "center",
    gap: "13px",
  },
  cardBody: {
    display: "grid",
    gap: "21px",
  },
  cardBodyBetsOptions: {
    display: "grid",
    gap: "13px",
  },
  cardHeaderMarket: { textAlign: "center", color: "whitesmoke" },
  cardHeader: {
    display: "grid",
    gridAutoFlow: "column",
    alignItems: "center",
    justifyContent: "space-between",
  },
  options: {
    display: "grid",
    justifyItems: "center",
    borderColor: "black",
    borderRadius: "13px",
    padding: "8px",
    border: "1px solid chocolate",
    cursor: "pointer",
  },
  optionSelected: {
    display: "grid",
    justifyItems: "center",
    backgroundColor: "#0C7714",
    borderRadius: "13px",
    padding: "8px",
    color: "white",
  },
  optionImg: {
    display: "grid",
    gridAutoFLow: "column",
    textAlign: "center",
    justifyItems: "center",
  },
});

import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import CardEvent from "./CardEvent";
import image from "../assets/iridian.png";
import * as App from "../App";

describe("CardEvent Component", () => {
  let mockBetsValue;

  beforeEach(() => {
    mockBetsValue = {
      betsSelection: [],
      setBetsSelection: jest.fn(),
      data: [
        {
          name: "Test Event 1",
          markets: [
            {
              name: "Market 1",
              selections: [
                {
                  name: "Selection 1",
                  price: 2.5,
                  imageUrl: image,
                  selected: false,
                },
                {
                  name: "Selection 2",
                  price: 3.0,
                  imageUrl: image,
                  selected: true,
                },
              ],
            },
          ],
        },
        {
          name: "Test Event 2",
          markets: [],
        },
      ],
      setData: jest.fn(),
    };

    jest.spyOn(App, "Bets").mockReturnValue(mockBetsValue);
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("renders CardEvent component correctly with test data", () => {
    const { getByText, queryByText } = render(<CardEvent />);
    expect(getByText("Test Event 1")).toBeDefined();
    expect(getByText("Market 1")).toBeDefined();
    expect(queryByText("Test Event 2")).toBeNull();
  });

  it("handles selection press and updates context", () => {
    const { getByText } = render(<CardEvent />);
    const selectionElement = getByText("Selection 1");
    fireEvent.press(selectionElement);
    expect(mockBetsValue.setBetsSelection).toHaveBeenCalledWith([
      {
        eventName: "Test Event 1",
        eventIndex: 0,
        marketName: "Market 1",
        marketIndex: 0,
        price: 2.5,
        imageUrl: image,
        selectionIndex: 0,
        selectionName: "Selection 1",
      },
    ]);
  });
});

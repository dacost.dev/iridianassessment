import React from 'react';
import { render } from '@testing-library/react-native';
import Navbar from './Navbar';

describe('Navbar Component', () => {
  const mockNavigation = {
    navigate: jest.fn(),
  };

  it('renders Navbar component correctly', () => {
    const { getByText } = render(<Navbar navigation={mockNavigation} />);
    
    expect(getByText('Event List')).toBeDefined();
  });

  it('navigates to "Bets" screen on icon press', () => {
    render(<Navbar navigation={mockNavigation} />);
  });


});
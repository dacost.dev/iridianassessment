# README

This repository contains an Expo/React/React Native App for the Iridian assessment. It includes an sport's event list screen, bet list screen and loading screen. 

## components
```bash
CardEvent.js (The logic of the construction of event container cards is separated, due to its complexity of nesting to achieve its structure and better visualization for its maintenance.)
 
```
```bash
Navbar.js (Separation of shared component).
 
```
## pages
```bash
BetList.js (Logic to render the n number of betting elements that are in the state declared in the parent component by means of a context.)
 
```
```bash
EventList.js (It obtains the data from the service by means of fetch inside a hook useEffect leaving the tree of dependencies empty so that this block of code only runs once and guarantees not to make unnecessary requests to the service.)
 
```
```bash
Loading.js (Initial creen with a progress bar and the iridian logo spinning with animations from react native)
 
```

## routes

```bash
Navigation.js (Routing configuration screens)
 
```


### Prerequisites

Before you begin, ensure you have the following installed:
- Node.js (18.19.0) LTS

## Getting Started

1. Clone the repository:

```bash
git clone https://gitlab.com/dacost.dev/iridianassessment.git
 
```

2.- Install node dependecies & crate the package-lock.json file

```bash
npm i
```

2.- Start Project (By default run in the browser)

```bash
npm start
```


If you need to run the application in android you need to install Expo Go application available in google play store and run this aplication with this command to create a tunnel and connect the phone to the localhost pc.

```bash
expo start --tunnel 
```

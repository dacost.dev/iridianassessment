import React, { useContext, createContext, useState } from "react";
import Navigation from "./routes/Navigation";

const BetsContext = createContext(null);

export const Bets = () => useContext(BetsContext);

export default function App() {
  const [betsSelection, setBetsSelection] = useState([]);
  const [data, setData] = useState([]);

  return (
    <BetsContext.Provider value={{ betsSelection, setBetsSelection, data, setData }}>
      <Navigation />
    </BetsContext.Provider>
  );
}

import { MaterialIcons } from "@expo/vector-icons";
import { Bets } from "../App";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Image,
} from "react-native";

export default function BetList({ navigation }) {
  const { setBetsSelection, betsSelection, data, setData } = Bets();

  return (
    <>
      <View style={styles.container}>
        <View style={styles.nav}>
          <Text style={styles.navTitle}>Bet List</Text>
          <TouchableHighlight onPress={() => navigation.goBack()}>
            <MaterialIcons name="close" size={21} color="white" />
          </TouchableHighlight>
        </View>
        {betsSelection.map((bet, betIndex) => {
          return (
            <View key={bet.eventName + betIndex} style={styles.containerBet}>
              <View style={styles.betList}>
                <Text testID="testEvent" style={styles.betItemText}>{bet.eventName}</Text>
                <Text testID="testMarket" style={styles.betItemText}>{bet.marketName}</Text>
                <View  style={styles.containerCardBet}>
                  <View style={styles.containerImageBet}>
                    <Image
                      source={bet.imageUrl}
                      style={{ width: 55, height: 55 }}
                    />
                  </View>
                  <View style={styles.containerInfoBet}>
                    <Text testID="testSelection" style={styles.betItemText}>{bet.selectionName}</Text>
                    <Text style={styles.betItemText}>{bet.price}</Text>
                  </View>
                  <View style={styles.containerInfoBet}>
                    <TouchableHighlight
                      testID="delete-icon"
                      onPress={() => {
                        data[bet.eventIndex].markets[
                          bet.marketIndex
                        ].selections[bet.selectionIndex].selected = false;
                        delete betsSelection[betIndex];
                        setData([...data]);
                        const bets = betsSelection.filter(
                          (x) => x !== undefined
                        );
                        setBetsSelection([...bets]);
                      }}
                    >
                      <MaterialIcons
                        name="delete"
                        size={21}
                        color="chocolate"
                      />
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
              <hr style={styles.hr} />
            </View>
          );
        })}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgb(14 16 16)",
    overflowY: "scroll",
  },
  nav: {
    display: "grid",
    justifyContent: "space-between",
    gridAutoFlow: "column",
    backgroundColor: "#142021",
    padding: "21px",
    position: "fixed",
    top: 0,
    width: "100%",
    zIndex: 1,
    borderBottomLeftRadius: "15px",
    borderBottomRightRadius: "15px",
  },
  containerCardBet: {
    display: "grid",
    gridAutoFlow: "column",
    alignItems: "center",
  },
  containerBet: {
    paddingTop: "100px",
    display: "grid",
    justifyContent: "center",
  },
  betList: { gap: "13px" },
  hr: {
    width: "100%",
    marginTop: "21px",
    marginBottom: "21px",
    borderColor: "#797979",
  },
  navTitle: { color: "chocolate" },
  betItemText: { color: "whitesmoke" },
});

import React, { useEffect, useRef } from "react";
import { View, StyleSheet, Animated, Easing, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Logo from "../assets/iridian.png";

const Loading = () => {
  const navigation = useNavigation();
  const spinValue = new Animated.Value(0);
  const progress = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.loop(
      Animated.timing(spinValue, {
        toValue: 1,
        duration: 5000,
        easing: Easing.linear,
        useNativeDriver: true,
      })
    ).start();
  }, [spinValue]);

  useEffect(() => {
    Animated.timing(progress, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: false,
    }).start(() => {
      navigation.replace("Events");
    });
  }, [progress]);

  const spin = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ["0deg", "360deg"],
  });

  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          transform: [{ rotate: spin }],
        }}
      >
        <Image source={Logo} style={{ width: 233, height: 233 }} />
      </Animated.View>
      <View
        style={{
          width: "100%",
          height: 20,
          backgroundColor: "#142021",
          borderRadius: 10,
          marginTop: 10,
        }}
      >
        <Animated.View
          style={{
            width: progress.interpolate({
              inputRange: [0, 1],
              outputRange: ["0%", "100%"],
            }),
            height: "100%",
            backgroundColor: "#c9c9c9",
            borderRadius: 10,
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    gap: 55,
    backgroundColor: "rgb(14 16 16)",
  },
});

export default Loading;

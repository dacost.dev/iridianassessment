import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
import Navbar from "../components/Navbar";
import CardEvent from "../components/CardEvent";
import { Bets } from "../App";
import dataMock from "../data.json";

export default function EventList({ navigation }) {
  const { data, setData } = Bets();

  useEffect(() => {
    // Uncomment the  next line if the endpoint doesnt work to get the data of the json file
    //setData(dataMock);
    fetch("https://run.mocky.io/v3/4c54ca9c-5b6a-4346-81d3-5431fcd55a40")
      .then((res) => res.json())
      .then((x) => {
        setData(x);
      });
  }, []);

  return (
    <>
      {data !== null ? (
        <View style={styles.container}>
          <Navbar navigation={navigation} />
          <View style={styles.cardContainer}>
            <CardEvent customData={data} />
          </View>
        </View>
      ) : null}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgb(14 16 16)",
  },
  cardContainer: {
    display: "grid",
    justifyContent: "center",
    alignItems: "center",
    padding: "16px",
    overflowY: "scroll",
    paddingTop: "100px",
    gap: "21px",
  }
});

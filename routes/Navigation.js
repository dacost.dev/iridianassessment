import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import EventList from "../pages/EventList";
import BetList from "../pages/BetList";
import Loading from "../pages/Loading";

const Stack = createStackNavigator();

const Navigation = () => {
  return (
    
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Loading">
        <Stack.Screen name="Events" component={EventList}  options={{ headerShown: false }}/>
        <Stack.Screen
          name="Bets"
          component={BetList}
          options={{
            headerShown: false,
            gestureEnabled: false,
            presentation: 'modal',
          }}
        />
        <Stack.Screen
          name="Loading"
          component={Loading}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
